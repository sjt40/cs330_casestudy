# Packages
import csv, json

class DataReader:

    @staticmethod
    def GetEdgeData():
        with open('data/edges.csv', mode='r') as file:
            edges = list(csv.reader(file))[1:]
            return edges

    @staticmethod
    def GetDriverData():
        with open('data/drivers.csv', mode='r') as file:
            drivers = list(csv.reader(file))[1:]
            return drivers

    @staticmethod
    def GetPassengerData():
        with open('data/passengers.csv', mode='r') as file:
            passengers = list(csv.reader(file))[1:]
            return passengers

    @staticmethod
    def GetNodeData():
        with open('data/node_data.json', 'r') as file:
            node_json_data = json.load(file)
            #node_data = []
            node_data = {}
            for node_id, coordinates in node_json_data.items():
                node_data[node_id] = (coordinates['lat'], coordinates['lon'])
                #longitude = coordinates['lon']
                #latitude = coordinates['lat']
                #node_data.append((node_id, latitude, longitude))
            return node_data

