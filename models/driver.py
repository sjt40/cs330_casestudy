class Driver:
    def __init__(self, id, time, source_lat, source_lon, profit, closest_node, num_trips):
        self.id = id
        self.time = time
        self.source_lat = source_lat
        self.source_lon = source_lon
        self.profit = profit
        self.closest_node = closest_node
        self.num_trips = num_trips

    def __lt__(self, other):
        return self.id < other.id
