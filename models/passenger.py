class Passenger:

    def __init__(self, id, time, source_lat, source_lon, dest_lat, dest_lon, source_node, dest_node, wait_time, pick_up_time):
        self.id = id
        self.time = time
        self.source_lat = source_lat
        self.source_lon = source_lon
        self.dest_lat = dest_lat
        self.dest_lon = dest_lon
        self.source_node = source_node
        self.dest_node = dest_node
        self.wait_time = wait_time
        self.pick_up_time = pick_up_time