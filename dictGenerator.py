from dataReader import DataReader

class DictGenerator:
    def generate_dict():
        edges = DataReader.GetEdgeData()
        d = {}
        index = 0
        for edge in edges:
            if edge[0] in d:
                d[edge[0]].append(index)
            else:
                d[edge[0]] = [index]
            index += 1
        return d