import time
from dataReader import DataReader
from modelMapper.modelMapper import ModelMapper
from tasks.t1_not_uber import T1NotUber
from tasks.t2_not_uber import T2NotUber
from tasks.t3_not_uber import T3NotUber
from tasks.t4_not_uber import T4NotUber
from tasks.t5_not_uber import T5NotUber
from homework.kcenter import KCenter
from homework.KMeans import KMeans
from dictGenerator import DictGenerator
from tasks.task_utils import find_closest_node, sort_nodes, binary_search

def main():

    graph_gen_start_time = time.time()
    graph = DictGenerator.generate_dict()
    graph_gen_end_time = time.time()
    print(f"Generate Graph -- Elapsed time: {graph_gen_end_time - graph_gen_start_time} seconds")

    edges = DataReader.GetEdgeData()
    nodes = DataReader.GetNodeData()
    
    
    driverData = DataReader.GetDriverData()
    passengerData = DataReader.GetPassengerData()

    drivers = ModelMapper.map_data_to_drivers(driverData)
    passengers = ModelMapper.map_data_to_passengers(passengerData)
     
    case_study(driverData, passengerData, graph, edges, nodes)
    homework_ten(passengers)

def case_study(driverData, passengerData, graph, edges, nodes):
    drivers = ModelMapper.map_data_to_drivers(driverData)
    passengers = ModelMapper.map_data_to_passengers(passengerData)
     
    t1_start_time = time.time()
    t1_not_uber = T1NotUber()
    t1_not_uber.main(passengers, drivers)
    t1_end_time = time.time()
    print(f"T1 -- Elapsed time: {round(t1_end_time - t1_start_time,2)} seconds")
    
    drivers = ModelMapper.map_data_to_drivers(driverData)
    passengers = ModelMapper.map_data_to_passengers(passengerData)

    t2_start_time = time.time()
    t2_not_uber = T2NotUber()
    t2_not_uber.main(passengers, drivers)
    t2_end_time = time.time()
    print(f"T2 -- Elapsed time: {round(t2_end_time - t2_start_time,2)} seconds")
    
    drivers = ModelMapper.map_data_to_drivers(driverData)
    passengers = ModelMapper.map_data_to_passengers(passengerData)

    t3_start_time = time.time()
    t3_not_uber = T3NotUber()
    t3_not_uber.main(passengers, drivers, graph, edges, nodes)
    t3_end_time = time.time()
    print(f"T3 -- Elapsed time: {round(t3_end_time - t3_start_time,2)} seconds or ~{(t3_end_time - t3_start_time)/60} minutes")
    
    drivers = ModelMapper.map_data_to_drivers(driverData)
    passengers = ModelMapper.map_data_to_passengers(passengerData)

    t4_start_time = time.time()
    t4_not_uber = T4NotUber()
    t4_not_uber.main(passengers, drivers, graph, edges, nodes)
    t4_end_time = time.time()
    print(f"T4 -- Elapsed time: {round(t4_end_time - t4_start_time,2)} seconds or ~{(t4_end_time - t4_start_time)/60} minutes")
    
    drivers = ModelMapper.map_data_to_drivers(driverData)
    passengers = ModelMapper.map_data_to_passengers(passengerData)

    t5_start_time = time.time()
    t5_not_uber = T5NotUber()
    t5_not_uber.main(passengers, drivers, graph, edges, nodes)
    t5_end_time = time.time()
    print(f"T5 -- Elapsed time: {round(t5_end_time - t5_start_time,2)} seconds or ~{(t5_end_time - t5_start_time)/60} minutes")

def homework_ten(passengers):
    #K Center

    print("K CENTER RUNS")

    print()
    k_2_start = time.time()
    k_center = KCenter()
    k_center.main(passengers, 2)
    k_2_end = time.time()
    print(f"K = 2 -- Empirical Run Time: {round(k_2_end - k_2_start,2)} seconds")
    print()
    k_4_start = time.time()
    k_center = KCenter()
    k_center.main(passengers, 4)
    k_4_end = time.time()
    print(f"K = 4 -- Empirical Run Time: {round(k_4_end - k_4_start,2)} seconds")
    print()
    k_6_start = time.time()
    k_center = KCenter()
    k_center.main(passengers, 6)
    k_6_end = time.time()
    print(f"K = 6 -- Empirical Run Time: {round(k_6_end - k_6_start,2)} seconds")
    print()
    k_8_start = time.time()
    k_center = KCenter()
    k_center.main(passengers, 8)
    k_8_end = time.time()
    print(f"K = 8 -- Empirical Run Time: {round(k_8_end - k_8_start,2)} seconds")
    print()

    # Random K Means
    print("RANDOM K MEANS RUNS")

    print()
    random_k_2_means_start = time.time()
    random_k_means = KMeans()
    run_1 = random_k_means.main(passengers, 2, 10, None)
    run_2 = random_k_means.main(passengers, 2, 10, None)
    run_3 = random_k_means.main(passengers, 2, 10, None)
    print(f"Sum of Squared Distances with K: 2: {(run_1 + run_2 + run_3) / 3}")
    random_k_2_means_end = time.time()
    print(f"Randomm K Means-- Empirical Run Time: {round(random_k_2_means_end - random_k_2_means_start,2)} seconds")

    print()
    random_k_means_4_start = time.time()
    random_k_means = KMeans()
    run_1 = random_k_means.main(passengers, 4, 10, None)
    run_2 = random_k_means.main(passengers, 4, 10, None)
    run_3 = random_k_means.main(passengers, 4, 10, None)
    print(f"Sum of Squared Distances with K: 4: {(run_1 + run_2 + run_3) / 3}")
    random_k_means_4_end = time.time()
    print(f"Randomm K Means-- Empirical Run Time: {round(random_k_means_4_end - random_k_means_4_start,2)} seconds")

    print()
    random_k_means_6_start = time.time()
    random_k_means = KMeans()
    run_1 = random_k_means.main(passengers, 6, 10, None)
    run_2 = random_k_means.main(passengers, 6, 10, None)
    run_3 = random_k_means.main(passengers, 6, 10, None)
    print(f"Sum of Squared Distances with K: 6: {(run_1 + run_2 + run_3) / 3}")
    random_k_means_6_end = time.time()
    print(f"Randomm K Means-- Empirical Run Time: {round(random_k_means_6_end - random_k_means_6_start,2)} seconds")

    print()
    random_k_means_8_start = time.time()
    random_k_means = KMeans()
    run_1 = random_k_means.main(passengers, 8, 10, None)
    run_2 = random_k_means.main(passengers, 8, 10, None)
    run_3 = random_k_means.main(passengers, 8, 10, None)
    print(f"Sum of Squared Distances with K: 8: {(run_1 + run_2 + run_3) / 3}")
    random_k_means_8_end = time.time()
    print(f"Randomm K Means-- Empirical Run Time: {round(random_k_means_8_end - random_k_means_8_start,2)} seconds")
    print()

    # K Means ++
    print("K MEANS ++ RUNS")

    k_center = KCenter()
    centers = k_center.main(passengers, 2)

    print()
    k_means_plus_2_start = time.time()
    k_means_plus = KMeans()
    run_1 = k_means_plus.main(passengers, 2, 10, centers)
    run_2 = k_means_plus.main(passengers, 2, 10, centers)
    run_3 = k_means_plus.main(passengers, 2, 10, centers)
    print(f"Sum of Squared Distances with K: 2: {(run_1 + run_2 + run_3) / 3}")
    k_means_plus_2_end = time.time()
    print(f"K Means ++ -- Empirical Run Time: {round(k_means_plus_2_end - k_means_plus_2_start ,2)} seconds")

    k_center = KCenter()
    centers = k_center.main(passengers, 4)
    print()
    k_means_plus_4_start = time.time()
    k_means_plus = KMeans()
    run_1 = k_means_plus.main(passengers, 4, 10, centers)
    run_2 = k_means_plus.main(passengers, 4, 10, centers)
    run_3 = k_means_plus.main(passengers, 4, 10, centers)
    print(f"Sum of Squared Distances with K: 4: {(run_1 + run_2 + run_3) / 3}")
    k_means_plus_4_end = time.time()
    print(f"K Means ++ -- Empirical Run Time: {round(k_means_plus_4_end - k_means_plus_4_start,2)} seconds")


    k_center = KCenter()
    centers = k_center.main(passengers, 6)
    print()
    k_means_plus_6_start = time.time()
    k_means_plus = KMeans()
    run_1 = k_means_plus.main(passengers, 6, 10, centers)
    run_2 = k_means_plus.main(passengers, 6, 10, centers)
    run_3 = k_means_plus.main(passengers, 6, 10, centers)
    print(f"Sum of Squared Distances with K: 6: {(run_1 + run_2 + run_3) / 3}")
    k_means_plus_6_end = time.time()
    print(f"K Means ++ - Empirical Run Time: {round(k_means_plus_6_end - k_means_plus_6_start,2)} seconds")


    k_center = KCenter()
    centers = k_center.main(passengers, 8)
    print()
    k_means_plus_8_start = time.time()
    k_means_plus = KMeans()
    run_1 = k_means_plus.main(passengers, 8, 10, centers)
    run_2 = k_means_plus.main(passengers, 8, 10, centers)
    run_3 = k_means_plus.main(passengers, 8, 10, centers)
    print(f"Sum of Squared Distances with K: 8: {(run_1 + run_2 + run_3) / 3}")
    k_means_plus_8_end = time.time()
    print(f"K Means ++ -- Empirical Run Time: {round(k_means_plus_8_end - k_means_plus_8_start ,2)} seconds")
    print()

if __name__ == "__main__":
    main()
