import random 
import math

from typing import List
from models.passenger import Passenger

class KMeans: 

    def __init__(self):
        self.driversEnRoute = []
        self.driverPassengerPairings = []

    def main(self, passengers: List[Passenger], k, iterations, centers):

        
        if(centers is None): 
            centers = set()
            # Add k random points as centers
            while len(centers) < k:
                start_point = random.choice(passengers)
                if (start_point.source_lat, start_point.source_lon) not in centers:
                    centers.add((start_point.source_lat, start_point.source_lon))
        
        # Assign points to their closest center
        center_map = self.findMinCenters(centers, passengers)
        
        # Repeat for arbitrary number of iterations
        for i in range(iterations):
            centers = set()
            for center in center_map:
                # Find average center point of cluster
                new_center = self.findNewCenter(center_map.get(center))
                centers.add(new_center)

            # Reset center_map with new list of centers
            center_map = self.findMinCenters(centers, passengers)

        return self.get_sum_squred_distance(center_map)

    def findMinCenters(self, centers, passengers):
        min_distances = {}

        for passenger in passengers:
            closest_center = None
            closest_distance = float('infinity')
            for center in centers:
                curr_distance = self.calculate_distance(center[0], center[1], passenger.source_lat, passenger.source_lon)
                if curr_distance < closest_distance:
                    closest_distance = curr_distance
                    closest_center = center
                    
            if closest_center not in min_distances:
                min_distances[closest_center] = []
            min_distances[closest_center].append(passenger)

        return min_distances
    
    def findNewCenter(self, passenger_cluster: List[Passenger]):
        total_lat = 0
        total_lon = 0

        for passenger in passenger_cluster:
            total_lat += passenger.source_lat
            total_lon += passenger.source_lon

        return (total_lat / len(passenger_cluster), total_lon / len(passenger_cluster))
    

    def get_sum_squred_distance(self, center_map):
        total = 0
        for center, passengers in center_map.items():
            squared_distance = 0
            for passenger in passengers:
                distance = self.calculate_distance(center[0], center[1], passenger.source_lat, passenger.source_lon)
                squared_distance += distance ** 2
            total += squared_distance

        return total
    
    def calculate_distance(self, lat1, lon1, lat2, lon2):
        # Haversine formula to convert coordinate distance to miles -- found online on stack overflow 
        R = 3959.87433 

        lat1_rad = math.radians(lat1)
        lon1_rad = math.radians(lon1)
        lat2_rad = math.radians(lat2)
        lon2_rad = math.radians(lon2)

        # Haversine formula
        dlon = lon2_rad - lon1_rad
        dlat = lat2_rad - lat1_rad
        a = math.sin(dlat / 2)**2 + math.cos(lat1_rad) * math.cos(lat2_rad) * math.sin(dlon / 2)**2
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

        distance = R * c
        return distance