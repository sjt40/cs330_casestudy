import random 
import math

from typing import List
from models.passenger import Passenger

class KCenter: 

    def __init__(self):
        self.driversEnRoute = []
        self.driverPassengerPairings = []

    def main(self, passengers: List[Passenger], k):
        print(f"COMPUTING FOR {k} CENTERS")

        # random.seed(1) # for reproducibility
        start_point = random.choice(passengers)

        centers = set()
        centers.add((start_point.source_lat, start_point.source_lon))
        new_center = (start_point.source_lat, start_point.source_lon)
        
        min_distances = [self.calculate_distance(p.source_lat, p.source_lon, start_point.source_lat, start_point.source_lon) for p in passengers]

        while(len(centers) < k):
            max_distance = 0
            max_distance_idx = -1
            for i in range(len(passengers)): # Look at all passengers 
                if ((passengers[i].source_lat, passengers[i].source_lon) not in centers): # Don't reuse centers
                    min_distances[i] = min(self.calculate_distance(passengers[i].source_lat, passengers[i].source_lon, new_center[0], new_center[1]), min_distances[i])
                    if min_distances[i] > max_distance: # If this is the furthest point from its closest center
                        max_distance = min_distances[i]
                        max_distance_idx = i

            new_center = (passengers[max_distance_idx].source_lat, passengers[max_distance_idx].source_lon)
            centers.add((passengers[max_distance_idx].source_lat, passengers[max_distance_idx].source_lon))
            
        print(f"DONE COMPUTING FOR {k} CENTERS")
        self.print_centers(centers, passengers)
        return centers

    def calculate_distance(self, lat1, lon1, lat2, lon2):
        # Haversine formula to convert coordinate distance to miles -- found online on stack overflow 
        R = 3959.87433 

        lat1_rad = math.radians(lat1)
        lon1_rad = math.radians(lon1)
        lat2_rad = math.radians(lat2)
        lon2_rad = math.radians(lon2)

        # Haversine formula
        dlon = lon2_rad - lon1_rad
        dlat = lat2_rad - lat1_rad
        a = math.sin(dlat / 2)**2 + math.cos(lat1_rad) * math.cos(lat2_rad) * math.sin(dlon / 2)**2
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

        distance = R * c
        return distance

    def print_centers(self, centers, passengers):
        furthest_point = 0
        for passenger in passengers:
            curr_distance = float('infinity')
            for center in centers:
                curr_distance = min(curr_distance, self.calculate_distance(passenger.source_lat, passenger.source_lon, center[0], center[1]))
            if(curr_distance > furthest_point):
                furthest_point = curr_distance
        print(f"Cost: {furthest_point}")