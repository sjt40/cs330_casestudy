from datetime import datetime

from models.driver import Driver
from models.passenger import Passenger

class ModelMapper:
    def map_data_to_drivers(data):
        drivers = []
        driver_id_counter = 1  # Starting ID
        date_format = "%m/%d/%Y %H:%M:%S"

        for row in data:
            # Assuming each row is an array like [time, sourceLat, sourceLon, profit, current_node]
            time = datetime.strptime(row[0], date_format)
            source_lat = float(row[1])
            source_lon = float(row[2])
            driver = Driver(driver_id_counter, time, source_lat, source_lon, 0, None, 0)
            drivers.append(driver)
            driver_id_counter += 1  # Increment ID for the next driver

        return drivers

    def map_data_to_passengers(passengerData):
        passengers = []
        pasenger_id_counter = 1  # Starting ID
        date_format = "%m/%d/%Y %H:%M:%S"

        for row in passengerData:
            # Assuming each row is an array like [time, sourceLat, sourceLon, dest_lat, dest_lon, source_node, dest_node, wait_time]
            time = datetime.strptime(row[0], date_format)
            source_lat = float(row[1])
            source_lon = float(row[2])
            dest_lat = float(row[3])
            dest_lon = float(row[4])
            passenger = Passenger(pasenger_id_counter, time, source_lat, source_lon, dest_lat, dest_lon, None, None, 0, 0)
            passengers.append(passenger)
            pasenger_id_counter += 1  # Increment ID for the next driver
            
        return passengers
