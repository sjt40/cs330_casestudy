# CS330_CaseStudy


# Important Takeaways from Case Study Assignment Outline

- Pickup and drop-off coordinates may not be vertices of the graph and may not even be on the road network, in which case trips should be from the closest network vertices to these locations.

- A matching algorithm can make a decision whenever an event happens, at which point it can either wait (do nothing) or assign an unmatched driver to an unmatched passenger.

# Python Modules

- The csv module can read from & write to csv files,
- The json module can read from & write to json files,
- The time module or the timeit module can be used to time/benchmark code execution,
- The heapq module provides an implementation of an array-based binary heap (in addition to basic built-in lists, sets, and dictionaries) 
- The datetime module may be useful for dates and times.