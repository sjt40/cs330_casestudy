from dataReader import DataReader
import heapq
import math
import csv

# Constant used to calculate a time to go a certain distance (2 would assumes an average speed of 30 miles per hour)
DISTANCE_TO_TIME = 2
# Constant used for minimum number of trips a driver will take before maybe logging off
MINIMUM_NUM_TRIPS = 7
# Probability of continuing to drive after minimum number of trips
CONTINUE_DRIVING_AFTER_QUOTA = 0.2

def coordinate_distance_to_miles(lat1, lon1, lat2, lon2):
    # Haversine formula to convert coordinate distance to miles -- found online on stack overflow 
    R = 3959.87433 

    lat1_rad = math.radians(lat1)
    lon1_rad = math.radians(lon1)
    lat2_rad = math.radians(lat2)
    lon2_rad = math.radians(lon2)

    # Haversine formula
    dlon = lon2_rad - lon1_rad
    dlat = lat2_rad - lat1_rad
    a = math.sin(dlat / 2)**2 + math.cos(lat1_rad) * math.cos(lat2_rad) * math.sin(dlon / 2)**2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    distance = R * c
    return distance

def find_closest_node(nodes, lat, lon):
    closest_node = None
    closest_distance = float('infinity')
    for node in nodes:
        distance = (lat - float(node[1][0])) ** 2 + (lon - float(node[1][1])) ** 2
        if distance < closest_distance:
            closest_distance = distance
            closest_node = node
    return closest_node[0]

def sort_nodes(nodes):
    return sorted(nodes.items(), key=lambda x: (x[1][0], x[1][1]))

def binary_search(sorted_nodes, target_latitude, target_longitude):
    low, high = 0, len(sorted_nodes) - 1

    while high - low > 10:
        mid = (low + high) // 2
        current_node = sorted_nodes[mid]
        closest_distance = float('infinity')
        current_latitude, current_longitude = current_node[1]

        # If the target is less than the current node, search in the left half
        if (current_latitude, current_longitude) > (target_latitude, target_longitude):
            high = mid - 1

        # If the target is greater than the current node, search in the right half
        else:
            low = mid + 1

    return sorted_nodes[low : high + 1] # return 10 closest nodes

def dijkstra(graph, start, end, edges, dateTime):
    # Initialize times 
    times = {vertex: float('infinity') for vertex in graph}
    times[start] = 0

    # Priority queue to keep track of the vertices with their times
    priority_queue = [(0, start)]

    while priority_queue:
        current_time, current_vertex = heapq.heappop(priority_queue)

        # If we find out target location, return time of shortest path
        if current_vertex == end:
            return times[current_vertex]
        
        for idx in graph[current_vertex]:
            
            length = float(edges[idx][2])
            if dateTime.day == 25: # 4/25/2014 corresponds to a weekday
                speed = float(edges[idx][3 + dateTime.hour])
            else:
                speed = float(edges[idx][27 + dateTime.hour])
                
            travel_time = (length / speed) * 60
            time = current_time + travel_time

            # Update the time and predecessor if a shorter path is found
            neighbor = edges[idx][1]

            if time < times[neighbor]:
                times[neighbor] = time
                heapq.heappush(priority_queue, (time, neighbor))

    return None
 
def AStar(graph, source, dest, edges, dateTime, nodes):
    
    # Initialize times
    times = {vertex: float('infinity') for vertex in graph}
    times[source] = 0

    dest_lat = nodes[dest][0]
    dest_lon = nodes[dest][1]
    # Priority queue to keep track of the vertices with their times
    priority_queue = [(0, source)]

    while priority_queue:
        _, current_vertex = heapq.heappop(priority_queue)
        current_time = times[current_vertex]  # Retrieve the actual travel time b/c queue stores cost with heuristic

        # If we are at target -> return total time
        if current_vertex == dest:
            return times[current_vertex]

        # Inspect all neighbors of current vertex
        for idx in graph[current_vertex]:
            neighbor = edges[idx][1]
            length = float(edges[idx][2])
            speed = float(edges[idx][3 + dateTime.hour]) if dateTime.day == 25 else float(edges[idx][27 + dateTime.hour])
            travel_time = (length / speed) * 60  # Convert to minutes

            total_time = current_time + travel_time

            current_lat = nodes[current_vertex][0]
            current_lon = nodes[current_vertex][1]
        
            straight_line_dist = coordinate_distance_to_miles(current_lat, current_lon, dest_lat, dest_lon)

            if total_time < times[neighbor]:
                times[neighbor] = total_time
                # Push with time estimate + distance estimate as heuristic
                heapq.heappush(priority_queue, (total_time + straight_line_dist, neighbor))

    return None


def get_match_statistics(self, task):
    processed_ids = set()
    num_drivers = 0
    num_passengers = 0
    driver_profits = 0
    passenger_wait_times = 0
    passenger_wait_times_list = []
    driver_profits_list = []
    pick_up_time = 0
    pick_up_time_list = []

    for pair in self.driverPassengerPairings:
        if pair[0].id not in processed_ids:
            driver_profits += pair[0].profit
            num_drivers += 1
        num_passengers += 1
        passenger_wait_times += pair[1].wait_time
        passenger_wait_times_list.append(pair[1].wait_time)
        driver_profits_list.append(pair[0].profit)
        pick_up_time_list.append(pair[1].pick_up_time)

    
    # Create CSV files 

    # PASSENGER WAIT TIME CSV
    with open(f'output/passenger_wait_times_{task}.csv', 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        for item in passenger_wait_times_list:
            csv_writer.writerow([item])
    
    # DRIVER PROFIT CSV
    with open(f'output/driver_profit_{task}.csv', 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        for item in driver_profits_list:
            csv_writer.writerow([item])

    # PICKUP TIME CSV
    with open(f'output/pickup_time_{task}.csv', 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)

        for item in pick_up_time_list:
            csv_writer.writerow([item])
    

    print(f"T{task} - AVERAGE PASSENGER WAIT TIME: {round(passenger_wait_times / num_passengers, 2)}")
    print(f"T{task} - AVERAGE DRIVER PROFIT: {round(driver_profits / num_drivers, 2)}")
    print()