import heapq
import math
import random
import csv
from typing import List
from queue import Queue
from datetime import datetime, timedelta

from models.driver import Driver
from models.passenger import Passenger
from . import task_utils

class T3NotUber: 

    def __init__(self):
        self.passengerQueue = Queue()
        self.driverPriorityQueue = []
        self.driversEnRoute = []
        self.driverPassengerPairings = []

    def main(self, passengers: List[Passenger], drivers: List[Driver], graph, edges, nodes):
        print("ENTERING T3 MAIN FUNCTION...\n")

        passengerInd = 0
        driverInd = 0

        # iterate through all passengers and drivers
        while passengerInd < len(passengers) and driverInd < len(drivers):
                # If passenger requested before driver became available
                if passengers[passengerInd].time < drivers[driverInd].time:
                    current_passenger = self.update_passenger_nodes(passengers[passengerInd], nodes)
                    self.passengerQueue.put(current_passenger)
                    current_time = current_passenger.time
                    passengerInd += 1
                # If driver becomes available before new passenger requested
                else:
                    current_driver = drivers[driverInd]
                    if current_driver.closest_node is None:
                        current_driver.closest_node = task_utils.find_closest_node(nodes.items(), current_driver.source_lat, current_driver.source_lon)
                    heapq.heappush(self.driverPriorityQueue, (0, current_driver))
                    current_time = current_driver.time
                    driverInd += 1

                # add any drivers that completed their trip back to driver queue
                self.checkDriverTripComplete(current_time)

                # Continue looping til driver queue or passenger queue is empty
                availableMatches = True
                while availableMatches: 
                    availableMatches = self.rideMatch(current_time, graph, edges)
                
        
        # self.printPairings()
        task_utils.get_match_statistics(self, 3)

    def update_passenger_nodes(self, current_passenger, nodes):
        current_passenger.source_node = task_utils.find_closest_node(nodes.items(), current_passenger.source_lat, current_passenger.source_lon)
        current_passenger.dest_node = task_utils.find_closest_node(nodes.items(), current_passenger.dest_lat, current_passenger.dest_lon)
        return current_passenger
    
    def rideMatch(self, current_time: datetime, graph, edges):
        if len(self.driverPriorityQueue) == 0 or self.passengerQueue.empty():
            return False
        
        updated_queue = []
        
        # Pop the longest waiting passenger
        passenger = self.passengerQueue.get()

        # Create new queue for current passenger
        while self.driverPriorityQueue:
            _, driver = heapq.heappop(self.driverPriorityQueue)
            pickup_time = self.calculate_pickup_time(driver, passenger, current_time, graph, edges)
            heapq.heappush(updated_queue, (pickup_time, driver))
        
        # Update priority queue sorted to estimated pickup time to passenger
        self.driverPriorityQueue = updated_queue

        # Get the closest driver
        pickup_time, closest_driver = heapq.heappop(self.driverPriorityQueue)
        trip_time = self.calculate_trip_time(passenger, current_time, graph, edges)
        
        # Estimate time to complete ride using straight line distance * 2 (randomly selected constant)
        drop_off_time = current_time + timedelta(minutes= pickup_time) + timedelta(minutes= trip_time)
        
        # Update driver info after ride completes
        closest_driver = self.update_driver_after_trip(passenger, closest_driver, drop_off_time)
        closest_driver.profit = closest_driver.profit + trip_time - pickup_time

        wait_time = drop_off_time - passenger.time
        passenger.wait_time = wait_time.total_seconds() / 60

        #pick up time setting
        passenger.pick_up_time = pickup_time

        # Add the driver to the driversEnRoute priority queue with the projected completion time
        heapq.heappush(self.driversEnRoute, (drop_off_time, closest_driver))
        
        # Add driver and passenger to driverPassengerPairings
        self.driverPassengerPairings.append((closest_driver, passenger))

        return True
    
    def checkDriverTripComplete(self, current_time: datetime):
        # Check if there are drivers in the queue and their drop off times have passed
        while self.driversEnRoute and self.driversEnRoute[0][0] < current_time:
            # Pop the drivers whose ride is complete
            _, driver = heapq.heappop(self.driversEnRoute)

            if driver.num_trips <= task_utils.MINIMUM_NUM_TRIPS:
                heapq.heappush(self.driverPriorityQueue, (0, driver))
            else:
                if random.random() <= task_utils.CONTINUE_DRIVING_AFTER_QUOTA:
                    # Add driver back to pick up queue
                    heapq.heappush(self.driverPriorityQueue, (0, driver))
    
    ## UPDATE FUNCTIONS --------------------------------
    def update_driver_after_trip(self, passenger, closest_driver, drop_off_time):
        closest_driver.source_lat = passenger.dest_lat
        closest_driver.source_lon = passenger.dest_lon
        closest_driver.closest_node = passenger.dest_node
        closest_driver.time = drop_off_time
        closest_driver.num_trips += 1
        return closest_driver
    
    ## HELPER FUNCTIONS --------------------------------
    def calculate_pickup_time(self, driver: Driver, passenger : Passenger, current_time, graph, edges):
        return task_utils.dijkstra(graph, driver.closest_node, passenger.source_node, edges, current_time)
    
    def calculate_trip_time(self, passenger : Passenger, current_time, graph, edges):
        return task_utils.dijkstra(graph, passenger.source_node, passenger.dest_node, edges, current_time)
    
    def printPairings(self):
         for pair in self.driverPassengerPairings:
              print(pair[0].id, pair[1].id)
    