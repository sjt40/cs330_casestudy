import random
import heapq

from datetime import datetime, timedelta
from typing import List
from queue import Queue

from models.driver import Driver
from models.passenger import Passenger
from . import task_utils

class T1NotUber: 

    def __init__(self):
        self.passengerQueue = Queue()
        self.driverQueue = Queue()
        self.driversEnRoute = []
        self.driverPassengerPairings = []

    def main(self, passengers: List[Passenger], drivers: List[Driver]):
        print("ENTERING T1 MAIN FUNCTION...\n")

        passengerInd = 0
        driverInd = 0

        # iterate through all passengers and drivers
        while passengerInd < len(passengers) and driverInd < len(drivers):
                # If passenger requested before driver became available, add to passenger queue
                if passengers[passengerInd].time < drivers[driverInd].time:
                    self.passengerQueue.put(passengers[passengerInd])
                    current_time = passengers[passengerInd].time
                    passengerInd += 1
                # If driver becomes available before new passenger requested, add to driver queue
                else: 
                    self.driverQueue.put(drivers[driverInd])
                    current_time = drivers[driverInd].time
                    driverInd += 1
                
                # add any drivers that completed their trip back to driver queue
                self.checkDriverTripComplete(current_time)

                # Continue looping until driver queue or passenger queue is empty
                availableMatches = True
                while availableMatches: 
                    availableMatches = self.rideMatch(current_time)
        
        #self.printPairings()
        task_utils.get_match_statistics(self, 1)
        print()
        
    def rideMatch(self, current_time):
            if self.driverQueue.empty() or self.passengerQueue.empty():
                return False
            
            # Pop the first available driver and the longest waiting passenger
            driver = self.driverQueue.get()
            passenger = self.passengerQueue.get()
            
            

            pick_up_distance = self.calculate_pickup_distance(driver, passenger)
            trip_distance = self.calculate_trip_distance(passenger)
            
            driver.profit = driver.profit + (trip_distance - pick_up_distance) * task_utils.DISTANCE_TO_TIME
            driver.num_trips += 1
                        
            drop_off_time = current_time + timedelta(minutes= task_utils.DISTANCE_TO_TIME * pick_up_distance) + timedelta(minutes= task_utils.DISTANCE_TO_TIME * trip_distance)
            heapq.heappush(self.driversEnRoute, (drop_off_time, driver))
            
            wait_time = drop_off_time - passenger.time
            passenger.wait_time = wait_time.total_seconds() / 60

            # Add pairing to global list
            self.driverPassengerPairings.append((driver, passenger))
            
            return True
    
    def checkDriverTripComplete(self, current_time: datetime):
        # Check if there are drivers in the queue and their drop off times have passed
        while self.driversEnRoute and self.driversEnRoute[0][0] < current_time:
            # Pop the drivers whose ride is complete
            _, driver = heapq.heappop(self.driversEnRoute)

            if driver.num_trips <= task_utils.MINIMUM_NUM_TRIPS:
                self.driverQueue.put(driver)
            else:
                if random.random() <= task_utils.CONTINUE_DRIVING_AFTER_QUOTA:
                    # Add driver back to pick up queue
                    self.driverQueue.put(driver)
    
    

    ## HELPER FUNCTIONS
    def calculate_pickup_distance(self, driver: Driver, passenger : Passenger):
        return task_utils.coordinate_distance_to_miles(driver.source_lat, driver.source_lon, passenger.source_lat, passenger.source_lon)
    
    def calculate_trip_distance(self, passenger : Passenger):
        return task_utils.coordinate_distance_to_miles(passenger.dest_lat, passenger.dest_lon, passenger.source_lat, passenger.source_lon)
    
    def printPairings(self):
        for pair in self.driverPassengerPairings:
              print(pair[0].id, pair[1].id)

