import heapq
import math
import random
from typing import List
from queue import Queue
from datetime import datetime, timedelta

from models.driver import Driver
from models.passenger import Passenger
from . import task_utils
from tasks.task_utils import find_closest_node, sort_nodes, binary_search

class T5NotUber: 

    def __init__(self):
        self.passengerPriorityQueue = []
        self.driverPriorityQueue = []
        self.driversEnRoute = []
        self.driverPassengerPairings = []

    def main(self, passengers: List[Passenger], drivers: List[Driver], graph, edges, nodes):
        print("ENTERING T5 MAIN FUNCTION...\n")

        passengerInd = 0
        driverInd = 0

        sorted_nodes = sort_nodes(nodes)
        
        # iterate through all passengers and drivers
        while passengerInd < len(passengers) and driverInd < len(drivers):
                # If passenger requested before driver became available
                if passengers[passengerInd].time < drivers[driverInd].time:
                    current_passenger = self.update_passenger_nodes(passengers[passengerInd], sorted_nodes)
                    trip_dist = self.calculate_trip_distance(current_passenger)
                    heapq.heappush(self.passengerPriorityQueue, (-trip_dist, current_passenger))
                    current_time = current_passenger.time
                    passengerInd += 1
                # If driver becomes available before new passenger requested
                else:
                    current_driver = drivers[driverInd]
                    if current_driver.closest_node is None:
                        closest_nodes_from_binary = binary_search(sorted_nodes, current_driver.source_lat, current_driver.source_lon)
                        current_driver.closest_node = task_utils.find_closest_node(closest_nodes_from_binary, current_driver.source_lat, current_driver.source_lon)
                    heapq.heappush(self.driverPriorityQueue, (0, current_driver))
                    current_time = current_driver.time
                    driverInd += 1

                # add any drivers that completed their trip back to driver queue
                self.checkDriverTripComplete(current_time)

                # Continue looping til driver queue or passenger queue is empty
                availableMatches = True
                while availableMatches: 
                    availableMatches = self.rideMatch(current_time, graph, edges, nodes)
                
        
        # self.printPairings()
        task_utils.get_match_statistics(self, 5)

    def update_passenger_nodes(self, current_passenger, nodes):
        closest_source_nodes_from_binary = binary_search(nodes, current_passenger.source_lat, current_passenger.source_lon)
        current_passenger.source_node = find_closest_node(closest_source_nodes_from_binary, current_passenger.source_lat, current_passenger.source_lon)
        
        closest_dest_nodes_from_binary = binary_search(nodes, current_passenger.dest_lat, current_passenger.dest_lon)
        current_passenger.dest_node = find_closest_node(closest_dest_nodes_from_binary, current_passenger.dest_lat, current_passenger.dest_lon)
        return current_passenger
    
    def rideMatch(self, current_time: datetime, graph, edges, nodes):
        if len(self.driverPriorityQueue) == 0 or len(self.passengerPriorityQueue)==0: # CHANGED
            return False
        
        updated_queue = []
        
        # Pop the passenger with the longest trip - CHANGED
        negated_trip_dist, passenger = heapq.heappop(self.passengerPriorityQueue)
        trip_dist = -negated_trip_dist

        # Create new queue for current passenger
        while self.driverPriorityQueue:
            _, driver = heapq.heappop(self.driverPriorityQueue)
            pickup_time = self.calculate_pickup_time(driver, passenger, current_time, graph, edges, nodes)
            heapq.heappush(updated_queue, (pickup_time, driver))
        
        # Update priority queue sorted to estimated pickup time to passenger
        self.driverPriorityQueue = updated_queue

        # Get the closest driver
        pickup_time, closest_driver = heapq.heappop(self.driverPriorityQueue)
        trip_time = self.calculate_trip_time(passenger, current_time, graph, edges, nodes)
        
        # Estimate time to complete ride using straight line distance * 2 (randomly selected constant)
        drop_off_time = current_time + timedelta(minutes= pickup_time) + timedelta(minutes= trip_time)
        
        # Update driver info after ride completes
        closest_driver = self.update_driver_after_trip(passenger, closest_driver, drop_off_time)
        closest_driver.profit = closest_driver.profit + trip_time - pickup_time
        
        wait_time = drop_off_time - passenger.time
        passenger.wait_time = wait_time.total_seconds() / 60

        #pick up time setting
        passenger.pick_up_time = pickup_time

        # Add the driver to the driversEnRoute priority queue with the projected completion time
        heapq.heappush(self.driversEnRoute, (drop_off_time, closest_driver))
        
        # Add driver and passenger to driverPassengerPairings
        self.driverPassengerPairings.append((closest_driver, passenger))

        return True
    
    def checkDriverTripComplete(self, current_time: datetime):
        # Check if there are drivers in the queue and their drop off times have passed
        while self.driversEnRoute and self.driversEnRoute[0][0] < current_time:
            # Pop the drivers whose ride is complete
            _, driver = heapq.heappop(self.driversEnRoute)

            if driver.num_trips <= task_utils.MINIMUM_NUM_TRIPS:
                heapq.heappush(self.driverPriorityQueue, (0, driver))
            else:
                if random.random() <= task_utils.CONTINUE_DRIVING_AFTER_QUOTA:
                    # Add driver back to pick up queue
                    heapq.heappush(self.driverPriorityQueue, (0, driver))
    
    ## UPDATE FUNCTIONS --------------------------------
    def update_driver_after_trip(self, passenger, closest_driver, drop_off_time):
        closest_driver.source_lat = passenger.dest_lat
        closest_driver.source_lon = passenger.dest_lon
        closest_driver.closest_node = passenger.dest_node
        closest_driver.time = drop_off_time
        closest_driver.num_trips += 1
        return closest_driver
    
    ## HELPER FUNCTIONS --------------------------------

    def calculate_trip_distance(self, passenger : Passenger):
        return task_utils.coordinate_distance_to_miles(passenger.dest_lat, passenger.dest_lon, passenger.source_lat, passenger.source_lon)
    

    def calculate_pickup_time(self, driver: Driver, passenger : Passenger, current_time, graph, edges, nodes):
        return task_utils.AStar(graph, driver.closest_node, passenger.source_node, edges, current_time, nodes)
    
    def calculate_trip_time(self, passenger : Passenger, current_time, graph, edges, nodes):
        return task_utils.AStar(graph, passenger.source_node, passenger.dest_node, edges, current_time, nodes)
    
    def printPairings(self):
         for pair in self.driverPassengerPairings:
              print(pair[0].id, pair[1].id)
