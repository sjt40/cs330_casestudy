import heapq
import math
import random
from typing import List
from queue import Queue
from datetime import datetime, timedelta

from models.driver import Driver
from models.passenger import Passenger
from . import task_utils

class T2NotUber: 

    def __init__(self):
        self.passengerQueue = Queue()
        self.driverPriorityQueue = []
        self.driversEnRoute = []
        self.driverPassengerPairings = []

    def main(self, passengers: List[Passenger], drivers: List[Driver]):
        print("ENTERING T2 MAIN FUNCTION...\n")

        passengerInd = 0
        driverInd = 0

        # iterate through all passengers and drivers
        while passengerInd < len(passengers) and driverInd < len(drivers):
                # If passenger requested before driver became available
                if passengers[passengerInd].time < drivers[driverInd].time:
                    self.passengerQueue.put(passengers[passengerInd])
                    current_time = passengers[passengerInd].time
                    passengerInd += 1
                # If driver becomes available before new passenger requested
                else:
                    heapq.heappush(self.driverPriorityQueue, (0, drivers[driverInd]))
                    current_time = drivers[driverInd].time
                    driverInd += 1

                # Continue looping until driver queue or passenger queue is empty
                availableMatches = True
                while availableMatches: 
                    availableMatches = self.rideMatch(current_time)
                
                # add any drivers that completed their trip back to driver queue
                self.checkDriverTripComplete(current_time)
        
        #self.printPairings()
        task_utils.get_match_statistics(self, 2)
        print()
    
    def rideMatch(self, current_time: datetime):
        if len(self.driverPriorityQueue) == 0 or self.passengerQueue.empty():
            return False
        
        updated_queue = []
        
        # Pop the longest waiting passenger
        passenger = self.passengerQueue.get()

        # Create new queue for current passenger
        while self.driverPriorityQueue:
            _, driver = heapq.heappop(self.driverPriorityQueue)
            pick_up_distance = self.calculate_distance(driver, passenger)
            heapq.heappush(updated_queue, (pick_up_distance, driver))
        
        # Update priority queue sorted to distance to passenger
        self.driverPriorityQueue = updated_queue

        # Get the closest driver
        pick_up_distance, closest_driver = heapq.heappop(self.driverPriorityQueue)
        trip_distance = self.calculate_trip_distance(passenger)

        # Estimate time to complete ride using straight line distance * 2 (randomly selected constant)
        drop_off_time = current_time + timedelta(minutes= task_utils.DISTANCE_TO_TIME * pick_up_distance) + timedelta(minutes= task_utils.DISTANCE_TO_TIME * trip_distance)
        
        # Update driver info after ride completes
        closest_driver.source_lat = passenger.dest_lat
        closest_driver.source_lon = passenger.dest_lon
        closest_driver.time = drop_off_time
        closest_driver.num_trips += 1
        closest_driver.profit = closest_driver.profit + (trip_distance - pick_up_distance) * task_utils.DISTANCE_TO_TIME
        
        #passenger.wait_time = pick_up_distance * task_utils.DISTANCE_TO_TIME
        wait_time = drop_off_time - passenger.time
        passenger.wait_time = wait_time.total_seconds() / 60

        # Add the driver to the driversEnRoute priority queue with the projected completion time
        heapq.heappush(self.driversEnRoute, (drop_off_time, closest_driver))
        
        # Add driver and passenger to driverPassengerPairings
        self.driverPassengerPairings.append((closest_driver, passenger))

        return True
    
    def checkDriverTripComplete(self, current_time: datetime):
        # Check if there are drivers in the queue and their drop off times have passed
        while self.driversEnRoute and self.driversEnRoute[0][0] < current_time:
            # Pop the drivers whose ride is complete
            _, driver = heapq.heappop(self.driversEnRoute)

            if driver.num_trips <= task_utils.MINIMUM_NUM_TRIPS:
                heapq.heappush(self.driverPriorityQueue, (0, driver))
            else:
                if random.random() <= task_utils.CONTINUE_DRIVING_AFTER_QUOTA:
                    # Add driver back to pick up queue
                    heapq.heappush(self.driverPriorityQueue, (0, driver))
    

    ## HELPER FUNCTIONS 
    def calculate_distance(self, driver: Driver, passenger : Passenger):
        return task_utils.coordinate_distance_to_miles(driver.source_lat, driver.source_lon, passenger.source_lat, passenger.source_lon)
    
    def calculate_trip_distance(self, passenger : Passenger):
        return task_utils.coordinate_distance_to_miles(passenger.dest_lat, passenger.dest_lon, passenger.source_lat, passenger.source_lon)
    
    def printPairings(self):
         for pair in self.driverPassengerPairings:
              print(pair[0].id, pair[1].id)

